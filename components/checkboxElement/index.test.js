import { fireEvent } from '@testing-library/react';
import { render } from '../../testUtils';
import CheckboxElement from './index';


/* test with price type props */
const priceTypeProps = {
  type: 'price',
  data: {
    low: 1,
    high: 100,
    text: 'Price between 1 to 100'
  },
  text: 'Price between 1 to 100'
}

const setupPriceType = () => {
  const utils = render(
    <CheckboxElement 
      text={priceTypeProps.text} 
      data={priceTypeProps.data}
      type={priceTypeProps.type}
    />
  )
  const input = utils.getByText(priceTypeProps.text)
  return {
    input,
    ...utils,
  }
}


test('Price filter checkbox change', () => {
  const { input } = setupPriceType();

  fireEvent.click(
    input, 
    { target: { checked: true } }
  )
  expect(input.checked).toBe(true)
  
  fireEvent.click(
    input, 
    { target: { checked: false } }
  )
  expect(input.checked).toBe(false)
})




/* test with category type props */
const categoryTypeProps = {
  type: 'category',
  data: 'Mens Clothing',
  text: 'Mens Clothing'
}

const setupCategoryType = () => {
  const utils = render(
    <CheckboxElement 
      text={categoryTypeProps.text} 
      data={categoryTypeProps.data}
      type={categoryTypeProps.type}
    />
  )
  const input = utils.getByText(categoryTypeProps.text)
  return {
    input,
    ...utils,
  }
}

test('Category filter checkbox change', () => {
  const { input } = setupCategoryType();

  fireEvent.click(
    input, 
    { target: { checked: true } }
  )
  expect(input.checked).toBe(true)
  
  fireEvent.click(
    input, 
    { target: { checked: false } }
  )
  expect(input.checked).toBe(false)
})
