import React from "react";
import { useDispatch } from "react-redux";
import { GET_FILTERED_PRODUCT } from "../../store/productList/types";
import styles from './CheckboxElement.module.css';

export default function CheckboxElement({ data, text, type }){
  const dispatch = useDispatch();

  const handleChange = ({ target }) => {
    dispatch({
      type: GET_FILTERED_PRODUCT,
      payload: {
        filterBy: type,
        filterValue: data,
        filterChecked: target.checked
      }
    });
  }
  
  return(
    <label className={styles.checkboxContainer}>
      <input type="checkbox" 
        onClick={handleChange} 
      />{text}
      <span className={styles.checkmark}></span>
    </label>
  )
}