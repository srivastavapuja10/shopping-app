import styled from "styled-components";

export const Card = styled.div`
  background: #FFFFFF;
  border: 1px solid #d7d7e9;
  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.08);
  border-radius: 5px;
  padding: 15px;
  cursor: pointer;
  transform: scale(1);
  transition: transform 0.2s ease-in-out;
  &:hover {
    transform: scale(1.05);
  }
`;

export const H5 = styled.h1`
  font-size: 18px;
  font-weight: bold;
  color: #333;
  margin-bottom: 20px;  
`;

export const H6 = styled(H5)`
  font-size: 14px;
`;

export const Text1 = styled.p`
  font-size: 14px;
  color: #333;
  margin-bottom: 0px;
`;

export const SearchBox = styled.input`
  width: 70%;
  height: 34px;
  border-radius: 7px;
  border: 1.4px solid #9090a7;
  outline: none;
  padding: 10px;
  font-size: 14px;
  color: #333;
  display: inline;
  margin-left: 20px;
  flex: 1;
`
