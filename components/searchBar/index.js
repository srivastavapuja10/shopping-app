import React from "react";
import debounce from 'lodash/debounce';
import { useDispatch } from "react-redux";
import { GET_SEARCHED_PRODUCT } from "../../store/productList/types";
import { SearchBox } from "../styledComponents";

export default function SearchBar(){
  const dispatch = useDispatch();
  const handleSearch = ({target}) => {
    dispatch({
      type: GET_SEARCHED_PRODUCT,
      payload: target.value
    })
  }

  return(
    <>
      <SearchBox 
        type="text" 
        placeholder="Search with title, category.." 
        onChange={debounce(handleSearch, 500)}
      />
    </>
  )
}