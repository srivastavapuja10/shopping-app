import { fireEvent } from '@testing-library/react';
import { render } from '../../testUtils';
import SearchBar from './index';

const setup = () => {
  const utils = render(<SearchBar />)
  const input = utils.getByPlaceholderText('Search with title, category..')
  return {
    input,
    ...utils,
  }
}

test('Product searching calls onChange handler', () => {
  const { input } = setup()
  fireEvent.change(
    input, 
    { target: { value: 'white' } }
  )
  expect(input.value).toBe('white')
})