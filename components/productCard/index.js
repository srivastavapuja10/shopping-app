import { Card, Text1, H5, H6} from "../styledComponents";

function ProductCard({ data }){
  const { image, title, price } = data;
  return(
    <Card style={{width: "21%", margin: "2%"}}>
      <div style={{height: "210px", width: "80%", objectFit: "contain", margin: "auto"}}>
        <img src={image} alt={title} width="100%" height="100%" />
      </div>
      <H6>{title}</H6>
      <H6>Rs.{price}</H6>
    </Card>
  )
}

export default ProductCard;