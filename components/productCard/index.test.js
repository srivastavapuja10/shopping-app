import { render, screen } from "@testing-library/react";
import ProductCard from "./index.js";


describe("Product Card component renders values", () => {
  it("renders without crashing", () => {
    let dataProp = {
      img: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg', 
      title: 'abc', 
      price: 100
    }
    render(<ProductCard data={dataProp} />);
    expect(screen.getByText('abc')).toBeInTheDocument()
    expect(screen.getByText('Rs.100')).toBeInTheDocument()
  });
});