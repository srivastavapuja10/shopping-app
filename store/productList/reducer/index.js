import { 
  GET_PRODUCT_LIST, 
  GET_CATEGORY_LIST,
  GET_FILTERED_PRODUCT,
  GET_PRICE_LIST,
  GET_SEARCHED_PRODUCT
} from '../types'

const initialProductState = {
  totalProducts: [],
  displayProducts: [],
  selectedFilters: {
    category:[],
    price: []
  },
  searchString: ''
}

export function productList(state = initialProductState, action) {
  switch (action.type) {
    case GET_PRODUCT_LIST:
      const { totalProducts, selectedFilters, searchString } = state;
      const filteredProduct = getFilteredProduct(
          [...totalProducts, ...action.payload], selectedFilters, searchString);
      return {
        ...state, 
        totalProducts: [...state.totalProducts, ...action.payload],
        displayProducts: [...filteredProduct],
      }
    case GET_FILTERED_PRODUCT:{
      let { filteredProducts, currentFilters } = setFilteredValue(state, action.payload);
      return{
        ...state,
        displayProducts: [...filteredProducts],
        selectedFilters: currentFilters
      }
    }
    case GET_SEARCHED_PRODUCT:{
      const { totalProducts, selectedFilters } = state;
      const filteredProduct = getFilteredProduct(totalProducts, selectedFilters, action.payload);
      return{
        ...state,
        searchString: action.payload,
        displayProducts: [...filteredProduct]
      }
    }
    default:
      return state
  }
}

export function categoryList(state = [], action) {
  switch (action.type) {
    case GET_CATEGORY_LIST:
      return [...action.payload]
    default:
      return state
  }
}

export function priceList(state = [], action) {
  switch (action.type) {
    case GET_PRICE_LIST:
      return [...action.payload]
    default:
      return state
  }
}

function setFilteredValue(state, payload){
  const { totalProducts, selectedFilters, searchString } = state;
  const { filterBy, filterValue, filterChecked } = payload;
  let filteredProducts = [];
  let currentFilters = [];
  if(filterChecked){ 
    currentFilters={
      ...selectedFilters, 
      [filterBy]: [...selectedFilters[filterBy], filterValue]
    }
  }else{ /* if filter is unchecked */
    if(filterBy === "category"){
      let categoryFilters=selectedFilters[filterBy].filter((el) => el !== filterValue)
      currentFilters={...selectedFilters, [filterBy]: categoryFilters}
    }else if(filterBy === "price"){
      let priceFilter = selectedFilters[filterBy].filter((el) => el.low !== filterValue.low)
      currentFilters={...selectedFilters, [filterBy]: priceFilter}
    }
  }
 
  filteredProducts = getFilteredProduct(totalProducts, currentFilters, searchString);
  return {
    filteredProducts,
    currentFilters
  };
}

/* Confused regarding mutiple filter and search approach 
    1. Build it in redux as doing currently.
    2. Build it as local state
    3. Build it as conditional rendering w.r.t check of selected filter and search field
*/
const getFilteredProduct = (totalProducts, currentFilters, searchString) => {
  let filteredProducts = totalProducts.filter((el) => {
    let categoryFilter =  !currentFilters['category'].length || 
        currentFilters['category'].includes(el['category']) ;
    let productPricePresent =  currentFilters['price'].filter((priceArr) => 
        el.price >= priceArr.low && el.price <= priceArr.high);
    let priceFilter = !currentFilters['price'].length || productPricePresent.length;
    let searchStringFound = 
      el.title.toLowerCase().includes(searchString.toLowerCase()) || 
      el.category.toLowerCase().includes(searchString.toLowerCase());
    return categoryFilter && priceFilter && searchStringFound ;
  })
  return filteredProducts;
}