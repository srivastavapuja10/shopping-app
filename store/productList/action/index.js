import { 
  GET_PRODUCT_LIST, 
  GET_CATEGORY_LIST,
  GET_FILTERED_PRODUCT,
  GET_PRICE_LIST
} from '../types';
import { API_ENDPOINT } from '../../../config';

export const getProductList = () => async (dispatch) => {
  try {
    const res = await fetch(API_ENDPOINT+`products`)
    const products = await res.json()
    dispatch({
      type: GET_PRODUCT_LIST,
      payload: products,
    });
    dispatch(getUniqueProductCategory(products));
    dispatch(getPriceList());
  } catch (err) {
    // console.error(err);
  }
};

export const getFilteredProduct = (filterBy, filterValue, filterChecked) => async (dispatch) => {
  try{
    dispatch({
      type: GET_FILTERED_PRODUCT,
      payload: {
        filterBy,
        filterValue,
        filterChecked
      }
    })
  }catch(err){
    console.error(err.response);
  }
}
  

export const getUniqueProductCategory = (products) => async (dispatch, getState) => {
  const uniqueItems = new Set();
  const { totalProducts } = getState().productList;
  const arr = [...products, ...totalProducts];
  arr.filter(el => {
    const duplicate = uniqueItems.has(el.category);
    uniqueItems.add(el.category);
    return !duplicate;
  });
  dispatch({
    type: GET_CATEGORY_LIST,
    payload: [...uniqueItems]
  })
}

export const getPriceList = () => async (dispatch, getState) => {
  console.log("getState ===>", getState())
  let { priceList } = getState();
  console.log("priceList ===>", priceList)
  if(priceList && priceList.length === 0){
    priceList = [];
    for(let i=0; i<1000; i=i+200){
      priceList.push({ low: i+1, high: i+200, text: `Between Rs.${i+1} to Rs.${i+200}`})
    }
    priceList.push({ low: 1000, high: 1000000, text: 'Greater than Rs.1000'})
    dispatch({
      type: GET_PRICE_LIST,
      payload: priceList
    })
  }
}