import Head from 'next/head'
import styles from '../styles/Home.module.css'
import ProductCard from '../components/productCard'
import { H5, Text1 } from '../components/styledComponents'
import { useDispatch, useSelector } from "react-redux";
import { wrapper } from '../store/store'
import { getProductList } from '../store/productList/action';
import CheckboxElement from '../components/checkboxElement';
import SearchBar from '../components/searchBar';
import useInfiniteScroll from '../hooks/useInfinteScroll';
import { useEffect } from 'react';

function Home() {
  const dispatch = useDispatch();
  const displayProducts = useSelector((state) => state.productList.displayProducts);
  const totalProducts = useSelector((state) => state.productList.totalProducts);
  const categoryList = useSelector((state) => state.categoryList);
  const priceList = useSelector((state) => state.priceList);
  const [isFetching, setIsFetching] = useInfiniteScroll(fetchMoreProducts);

  function fetchMoreProducts(){
    dispatch(getProductList());
  }

  useEffect(() => {
    /* since totalProduct length is only changing when something is getting fetched */
    setIsFetching(false);
  },[totalProducts.length])

  return (
    <div className={styles.container}>
      <Head>
        <title>Shopping App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.headerContainer}>
        <H5>SHOPPY</H5>
        <SearchBar list={displayProducts} />
      </div>
      <div className={styles.mainContainer}>
        <div className={styles.leftContainer}>
          <H5>FILTERS</H5>
          <H5>Category</H5>
          <div className={styles.mb30}>
            {
              categoryList.map((data, index) => 
                <CheckboxElement 
                  text={data} 
                  data={data} 
                  key={index} 
                  type={"category"}
                /> 
              )
            }
          </div>
          <H5>Price</H5>
            <div className={styles.mb30}>
            {
              priceList.map((data, index) => 
                <CheckboxElement 
                  text={data.text} 
                  data={data} 
                  key={index} 
                  type={"price"}
                /> 
              )
            }
          </div>
        </div>
        <div className={styles.rightContainer}>
          <div className={styles.cardContainer}>
            {
              displayProducts.map((data, index) => 
                <ProductCard data={data} key={index} />
              )
            }
          </div>
          <H5 className={styles.textCenter}>{isFetching ? "Loading items ..." : "" }</H5>
        </div>
      </div>
    </div>
  )
}


// // This function gets called at build time
// export async function getStaticProps() {
//   // Call an external API endpoint to get posts
//   const dispatch = useDispatch();
//   const res = await fetch('https://fakestoreapi.com/products')
//   const products = await res.json()

//   // By returning { props: { posts } }, the Blog component
//   // will receive `posts` as a prop at build time
//   return {
//     props: {
//       products,
//     },
//   }
// }


export const getStaticProps = wrapper.getStaticProps(async ({ store }) => {
  console.log("called here ===>", store)
  await store.dispatch(getProductList())
})

export default Home;